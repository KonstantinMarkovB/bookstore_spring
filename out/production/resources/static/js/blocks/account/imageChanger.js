'use strict';
  var makeImageChangable = (function () {

    return function (wrapper, entityIdContainer, action) {
      var
        _wrapper = document.querySelector(wrapper),
        _image = _wrapper.querySelector(".account__img"),
        _configControll = _wrapper.querySelector(".account__config-img"),
        entityId = document.getElementById(entityIdContainer).value;

        _configControll.onclick = function(){
          var form = document.querySelector(".imageSendForm1234567634234");
          var imageFile;
          if(!form){
            form = document.createElement("form");
            form.style = "display: none;";
            form.setAttribute("method", "post");
            form.setAttribute("action", action);
            form.setAttribute("enctype", "multipart/form-data");
            form.classList.add("imageSendForm1234567634234")

            var userId = document.createElement("input");
            userId.setAttribute("type", "hidden")
            userId.setAttribute("name", "id")
            userId.setAttribute("value", entityId)
            

            imageFile = document.createElement("input");
            imageFile.setAttribute("type", "file");
            imageFile.setAttribute("name", "image");
            imageFile.classList.add('imageSendForm1234567634234imagefile')

            imageFile.onchange =function(){
              sendDataFromModalForm('.imageSendForm1234567634234', function(imgHref){
                _image.setAttribute("src", imgHref);
              });
            }
  
            form.appendChild(userId);
            form.appendChild(imageFile);
            document.body.appendChild(form);
          } else {
            imageFile = form.querySelector('.imageSendForm1234567634234imagefile');
          }

          imageFile.click();
        }
    }
  }());
