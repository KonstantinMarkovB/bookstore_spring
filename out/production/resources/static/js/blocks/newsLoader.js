'use strict';
    var newConfig = (function () {

        var _newContainer;

      var showNews = function(news, container){
        container.innerHTML = "";
        news.forEach(item => {
          var title = item.title;
          var content = item.content;
          var date = item.date;
          var image = item.image;
          var fullNewHref = "/new/get?id=" + item.id;

          var cont = document.createElement('div');
          cont.classList.add('news__item');
          cont.classList.add('sm-12');
          cont.classList.add('md-4');
          cont.classList.add('lg-3');
          cont.classList.add('xl-2');
          cont.setAttribute('itemscope', '');
          cont.setAttribute('itemtype', 'https://schema.org/NewsArticle');
          

          //itemscope itemtype="https://schema.org/Book"

          var new_ = document.createElement('div');
          new_.classList.add('new');

          var author_no = document.createElement('meta');
          author_no.setAttribute('itemprop', 'author');
          author_no.setAttribute('content', 'Admin')
          cont.appendChild(author_no);

          var publisher = document.createElement('div');
          publisher.setAttribute('itemprop', 'publisher');
          publisher.setAttribute('itemscope', '');
          publisher.setAttribute('itemtype', "https://schema.org/Organization")
          var name_pub= document.createElement('meta');
          name_pub.setAttribute('itemprop', 'name')
          name_pub.setAttribute('content', 'Admin')

          var logo_pub= document.createElement('div');
          logo_pub.setAttribute('itemprop', 'logo')
          logo_pub.setAttribute('itemscope', '');
          logo_pub.setAttribute('itemtype', "https://schema.org/ImageObject")
          var logo_url= document.createElement('meta');
          logo_url.setAttribute('itemprop', 'url')
          logo_url.setAttribute('content', '/imgstore/user/2.jpeg')
          logo_pub.appendChild(logo_url);

          
          publisher.appendChild(name_pub);
          publisher.appendChild(logo_pub);
          cont.appendChild(publisher);


          var newCloseForm = document.createElement('a');
          newCloseForm.classList.add('new__close-form');
          newCloseForm.setAttribute('href', '#0');

          var newCloseFormImg = document.createElement('div');
          newCloseFormImg.classList.add('new__close-form-img');

          var new__link = document.createElement('a');
          new__link.classList.add('new__link');
          new__link.setAttribute('href', fullNewHref);

          var newIconContainer = document.createElement('div');
          newIconContainer.classList.add('new__icon-container');

          var newIcon = document.createElement('img');
          newIcon.classList.add('new__icon');
          newIcon.setAttribute('src', image);
          newIcon.setAttribute('alt', title)
          newIcon.setAttribute('itemprop', 'image')
          
          var newContentContainer = document.createElement('div');
          newContentContainer.classList.add('new__content-container');

          var new_content = document.createElement('div');
          new_content.classList.add('new_content');
          new_content.innerHTML = title;
          new_content.setAttribute('itemprop', 'headline')

          var new_date = document.createElement('div');
          new_date.classList.add('new_date');
          new_date.innerHTML = date;
          new_date.setAttribute('itemprop', 'datePublished')


          var newFullPage = document.createElement('a');
          newFullPage.setAttribute('href', fullNewHref);
          newFullPage.innerHTML="Full page";
          setMessage('new.full-page', newFullPage)

          var new_fullText = document.createElement('div');
          new_fullText.classList.add('new_full-text');
          new_fullText.innerHTML = content;// + '<hr><a href=\'' + fullNewHref + '\'>dsdfsd</a>';
          new_fullText.setAttribute('itemprop', 'text')
          new_fullText.appendChild(document.createElement('hr'))
          new_fullText.appendChild(newFullPage);

          cont.appendChild(new_);
          new_.appendChild(newCloseForm);
          new_.appendChild(new__link);
          newCloseForm.appendChild(newCloseFormImg);
          new__link.appendChild(newIconContainer);
          new__link.appendChild(newContentContainer);
          newIconContainer.appendChild(newIcon);
          newContentContainer.appendChild(new_content);
          newContentContainer.appendChild(new_date);
          newContentContainer.appendChild(new_fullText);

          
          container.appendChild(cont);
        })
      }

      var loadNews = function(page = 0){
        newAjaxRequest("/new/all/json?countOnPage=12&pageNum=" + page, function(result){
          var res_ = JSON.parse(result);
          showNews(res_.content, _newContainer);
          showPagination(res_, 
            function(page){
              loadNews(page)
          }, function(page){
            loadNews(page)
          });

          // setNewsListener('.news');
        });
      }

      
      return function (container) {
        var _newItems = document.querySelector(container);
        _newContainer = _newItems.querySelector('.news__container');

          loadNews();

      }
    }());
