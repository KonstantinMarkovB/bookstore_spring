'use strict';
    var setNewsListener = (function () {
        return function (selector) {
            var
             _mainElement = document.querySelector(selector),
             _newsContainer = _mainElement.querySelector('.news__container'),
             _news = _mainElement.querySelectorAll('.news__item'),
             _items = [];

             _news.forEach(function (item) {
              _items.push({ 
                item: item,
                new: item.querySelector('.new'),
                close: item.querySelector('.new__close-form'),
                link: item.querySelector('.new__link'),
                text: item.querySelector('.new_full-text'),
              });
            });
    

            var _openModal = function (item){
              return function(){
                item.item.classList.add('new__item_modal');
                item.new.classList.add('new_modal');
                item.close.classList.add('new_modal__close-form');
                item.link.classList.add('new__link_modal');
                item.text.classList.add('new_full-text_modal');
              }
            };
            var _closeModal = function (item){
              return function(){
                item.item.classList.remove('new__item_modal');
                item.new.classList.remove('new_modal');
                item.close.classList.remove('new_modal__close-form');
                item.link.classList.remove('new__link_modal');
                item.text.classList.remove('new_full-text_modal');
              }
            }

            _items.forEach(function (item) {
              item.link.onclick = _openModal(item);
              item.close.onclick = _closeModal(item);
            });
        }
    })();