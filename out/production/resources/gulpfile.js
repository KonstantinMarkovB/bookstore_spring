var gulp = require('gulp'); 
gulp.task('autoprefixer', () => {
    const autoprefixer = require('autoprefixer')
    const sourcemaps = require('gulp-sourcemaps')
    const postcss = require('gulp-postcss')
  
    return gulp.src('./static/css/*.css')
      .pipe(sourcemaps.init())
      .pipe(postcss([ autoprefixer() ]))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./dest'))
  })

  gulp.task('lint-css', function lintCssTask() {
    const gulpStylelint = require('gulp-stylelint');
   
    return gulp
      .src('./static/css/*.css')
      .pipe(gulpStylelint({
        reporters: [
          {formatter: 'string', console: true}
        ]
      }));
  });

  gulp.task('default', ['autoprefixer']);