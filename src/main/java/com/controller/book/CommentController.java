package com.controller.book;

import com.model.Book;
import com.model.Comment;
import com.model.User;
import com.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Timestamp;
import java.util.Date;

@Controller
public class CommentController {
    @Autowired
    CommentRepository commentRepository;
    private String errorMess = "comment.error";

    @PostMapping(value = "/add_comment")
    public String add_comment(@RequestParam Integer bookId, @RequestParam String content,
                              Authentication authentication) {
        Book book = new Book();
        book.id = bookId;
        User user = (User) authentication.getPrincipal();

        Comment comment = new Comment();
        comment.book = book;
        comment.content = content;
        comment.date = new Timestamp(new Date().getTime());
        comment.user = user;

        try {
            commentRepository.save(comment);
            return "redirect:/book/card.html?id=" + bookId;
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/book/card.html?id=" + bookId + "&error=" + errorMess;

        }


    }
}
