package com.controller.book;

import com.model.Order;
import com.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;

    @GetMapping(value = "/order/all/json", produces = "application/json")
    @ResponseBody
    public Iterable<Order> adminPanel(
            @RequestParam(value = "id") Integer userId,
            @RequestParam(defaultValue = "0", required = false) Integer pageNum,
            @RequestParam(defaultValue = "3", required = false) Integer countOnPage) {

        return orderRepository.findByUserId(userId, PageRequest.of(pageNum, countOnPage));
    }

}
