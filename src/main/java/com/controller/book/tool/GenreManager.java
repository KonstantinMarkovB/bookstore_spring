package com.controller.book.tool;

import com.model.Book;
import com.model.BookGenre;
import com.model.Genre;
import com.repository.BookGenreRepository;
import com.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class GenreManager {
    @Autowired
    BookGenreRepository bookGenreRepository;

    @Autowired
    GenreRepository genreRepository;

    public void update(Book book, Integer[] genreIdList) {
        List<BookGenre> oldGenres = book.genres;
        for (BookGenre bookGenre : oldGenres) {
            bookGenre.book = book;
        }
        bookGenreRepository.deleteAll(oldGenres);

        insert(book, genreIdList);
    }

    public void insert(Book book, Integer[] genreIdList) {
        if (genreIdList != null) {
            Iterable<Genre> genreList = genreRepository.findAllById(Arrays.asList(genreIdList));
            List<BookGenre> bookGenres = new ArrayList<>();

            for (Genre g : genreList) {
                BookGenre bg = new BookGenre();
                bg.genre = g;
                bg.book = book;
                bookGenres.add(bg);
            }

            bookGenreRepository.saveAll(bookGenres);
        }

    }

}
