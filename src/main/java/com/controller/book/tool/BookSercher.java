package com.controller.book.tool;

import com.model.Author;
import com.model.Book;
import com.model.BookGenre;
import com.repository.AuthorRepository;
import com.repository.BookGenreRepository;
import com.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class BookSercher {

    @Autowired
    private BookRepository repository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookGenreRepository bookGenreRepository;

    public Page<Book> search(Integer pageNum, Integer countOnPage, Integer[] genresId, String text) {
        Page<Book> books;
        if (genresId != null) {
            // Поиск книг по жанрам
            Iterable<BookGenre> genres = bookGenreRepository.findByGenreIdIn(Arrays.asList(genresId));

            ArrayList<Integer> bookIdList = new ArrayList<>();

            for (BookGenre genre : genres) {
                bookIdList.add(genre.book.id);
            }

            if (text != null) {
                Iterable<Author> authors = authorRepository.findByNameContaining(text);
                ArrayList<Integer> authorsIdList = new ArrayList<>();
                for (Author a : authors)
                    authorsIdList.add(a.id);

                books = repository.findByIdInAndNameContainingOrAuthorIdInOrderByDateDesc(
                        bookIdList, text, authorsIdList, PageRequest.of(pageNum, countOnPage));
            } else {
                books = repository.findByIdInOrderByDateDesc(bookIdList, PageRequest.of(pageNum, countOnPage));
            }

        } else {
            if (text != null) {
                Iterable<Author> authors = authorRepository.findByNameContaining(text);
                ArrayList<Integer> authorsIdList = new ArrayList<>();
                for (Author a : authors)
                    authorsIdList.add(a.id);

                books = repository.findByNameContainingOrAuthorIdInOrderByDateDesc(text, authorsIdList, PageRequest.of(pageNum, countOnPage));
            } else {
                books = repository.findByOrderByDateDesc(PageRequest.of(pageNum, countOnPage));
            }
        }
        return books;
    }
}
