package com.controller.book.tool;

import com.model.Book;
import com.model.BookImage;
import com.repository.BookImagesRepository;
import com.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Component
public class ImagesInserter {
    @Value("${book.img.default}")
    private String DEFAULT_IMAGE;

    @Autowired
    private BookImagesRepository bookImagesRepository;

    @Autowired
    private StorageService storageService;

    public void insert(Book book, MultipartFile[] imagesNew) {
        List<BookImage> images = new ArrayList<>();
        for (int i = 0; i < imagesNew.length; i++) {
            if (!imagesNew[i].isEmpty()) {
                BookImage image = new BookImage();
                image.book = book;
                image.path = storageService.uploadFile(imagesNew[i], book.id + "_" + i, storageService.BOOK);
                images.add(image);
            }
        }

        if (images.size() == 0) {
            BookImage bookImage = new BookImage();
            bookImage.book = book;
            bookImage.path = DEFAULT_IMAGE;
            images.add(bookImage);
        }
        bookImagesRepository.saveAll(images);
    }

}
