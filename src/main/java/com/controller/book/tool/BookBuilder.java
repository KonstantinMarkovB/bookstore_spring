package com.controller.book.tool;

import com.model.Author;
import com.model.Book;
import com.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class BookBuilder {
    @Autowired
    private AuthorRepository authorRepository;

    public Book build() {
        Book book = new com.model.Book();
        book.author = new Author();
        book.genres = new ArrayList<>();
        return book;
    }

    public void build(Book book, Integer authorId, Integer count, String description, String name, String price) {
        book.author = authorRepository.findById(authorId).get();
        ;
        book.count = count;
        book.description = description;
        book.name = name;
        book.price = Integer.parseInt(price.replace(".", ""));
    }

    public Book build(Integer authorId, Integer count, String description, String name, String price) {
        Book book = new Book();
        book.author = authorRepository.findById(authorId).get();
        ;
        book.count = count;
        book.description = description;
        book.name = name;
        book.price = Integer.parseInt(price.replace(".", ""));
        book.date = new Date();
        return book;
    }


}
