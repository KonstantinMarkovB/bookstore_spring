package com.controller.book;

import com.model.Genre;
import com.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GenreController {
    @Autowired
    private GenreRepository genreRepository;

    @GetMapping(value = "/genres/json", produces = "application/json")
    public @ResponseBody
    Iterable<Genre> allJson() {
        return genreRepository.findAll();
    }


}
