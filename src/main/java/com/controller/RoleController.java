package com.controller;

import com.model.Role;
import com.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/role")
public class RoleController {
    @Autowired
    private RoleRepository roleRepository;


    @GetMapping(value = "/all/json", produces = "application/json")
    public @ResponseBody
    Iterable<Role> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                              @RequestParam(defaultValue = "50", required = false) Integer countOnPage){
        return roleRepository.findAll(PageRequest.of(pageNum, countOnPage, new Sort("id")));
    }

}
