package com.controller.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

@Controller
public class LanguageController {
//    /change-language

    @RequestMapping(value = "/change-language", method = RequestMethod.GET)
    public String getFile(HttpServletResponse response) {
//        Locale locale= new Locale.Builder().setLanguage("ru").build();
//        LocaleContextHolder.setLocale(locale);
//        LocaleContextHolder.setDefaultLocale(locale);
        return "redirect:index.html";
    }
}
