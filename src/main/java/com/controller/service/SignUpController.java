package com.controller.service;

import com.model.Role;
import com.model.User;
import com.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SignUpController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/signup")
    public String signup(@RequestParam String name,
                         @RequestParam String email,
                         @RequestParam String password) {
        User occur = userRepository.findByEmail(email);
        if (occur != null) {
            return "redirect:/index.html?signup=true&error=error.login-busy";
        }


        User user = new User();
        user.name = name;
        user.email = email;
        user.password = password;
        user.image = "/imgstore/user/default.jpeg";
        user.role = new Role();
        user.role.setLabel(Role.ROLE_USER);

        userRepository.save(user);


        return "redirect:/index.html?login=true";
    }

}
