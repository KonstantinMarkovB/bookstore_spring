package com.controller;


import com.model.Book;
import com.model.Order;
import com.model.User;
import com.repository.BookRepository;
import com.repository.OrderRepository;
import com.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Timestamp;
import java.util.Date;

@Controller
public class UserOrderController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    OrderRepository orderRepository;
    private String successMess = "order.success";
    private String errorMess = "order.error";
    @Autowired
    private MessageSource messageSource;

    @PostMapping(value = "/new_user_order")
    public String newUserOrder(@RequestParam Integer bookId, Authentication authentication) {
        Book book = bookRepository.findById(bookId).get();
        User user = (User) authentication.getPrincipal();

        Order order = new Order();
        order.price = book.price;
        order.book = book;
        order.date = new Timestamp(new Date().getTime());
        order.user = user;

        try {
            orderRepository.save(order);
            return "redirect:/book/card.html?id=" + bookId + "&success=" + successMess;
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/book/card.html?id=" + bookId + "&error=" + errorMess;

        }


    }


}
