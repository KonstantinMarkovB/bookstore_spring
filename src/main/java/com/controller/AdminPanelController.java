package com.controller;

import com.model.Role;
import com.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/admin-panel")
public class AdminPanelController {

    @GetMapping
    public String adminPanel(Model model, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
//        User user = new User();
//        user.id = 1;
//        user.role = new Role();
//        user.password="ss";
//        user.email="ssss";
//        user.image="/imgstore/user/1.jpeg";
//        user.name = "name";
//
        model.addAttribute("user", user);
        return "adminpanel/adminpanel";
    }

}
