package com.controller;

import com.model.Author;
import com.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Controller
@RequestMapping(path="/author")
public class AuthorController {
    @Autowired
    private AuthorRepository repository;

    @Autowired
    private MessageSource messageSource;


    @GetMapping("/form/forexist")
    public String getItem(@RequestParam Integer id, Model model){
        Author author = repository.findById(id).get();
        model.addAttribute("author", author);
        model.addAttribute("action", "/author/update");
        return "modalwindow/author/author";
    }

    @GetMapping("/form/fornew")
    public String newItem(Model model){
        model.addAttribute("author", new Author());
        model.addAttribute("action", "/author/insert");
        return "modalwindow/author/author";
    }

    @PostMapping("/update")
    @ResponseBody
    public String update(@RequestParam Integer id,
                         @RequestParam String name){

        Author author = repository.findById(id).get();
        author.name = name;
        repository.save(author);

        return "ok";
    }


    //TODO Добавление автора не работает
    @PostMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam String name){

        Author author = new Author();
        author.name = name;
        repository.save(author);

        return "ok";
    }


    @GetMapping(value = "/all/json", produces = "application/json")
    public @ResponseBody
    Iterable<Author> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                                @RequestParam(defaultValue = "50", required = false) Integer countOnPage){
        return repository.findAll(PageRequest.of(pageNum, countOnPage, new Sort("id")));
    }

    @GetMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Integer id){
        try {
            repository.deleteById(id);
        } catch (Exception e){
            return messageSource.getMessage("error.hasreference",null, LocaleContextHolder.getLocale());
        }
        return "ok";
    }

}
