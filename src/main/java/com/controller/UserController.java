package com.controller;

import com.model.Delivery;
import com.model.Role;
import com.model.User;
import com.repository.DeliveryRepository;
import com.repository.RoleRepository;
import com.repository.UserRepository;
import com.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Controller
@RequestMapping(path="/user")
@Validated
public class UserController {
    private static final String MAIL_PATTERN = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";

    Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private DeliveryRepository deliveryRepository;


    @GetMapping("/form/forexist")
    public String getItem(@RequestParam Integer id, Model model){
        User user = userRepository.findById(id).get();
        model.addAttribute("user", user);

        Iterable<Role> roles = roleRepository.findAll();
        model.addAttribute("roles", roles);

        model.addAttribute("action", "/user/update");

        return "modalwindow/user/user";
    }

    @GetMapping("/form/fornew")
    public String newItem(Model model){
        User user = new User();
        user.role  = new Role();
        user.image = "/imgstore/user/default.jpeg";
        model.addAttribute("user", user);

        Iterable<Role> roles = roleRepository.findAll();
        model.addAttribute("roles", roles);

        model.addAttribute("action", "/user/insert");

        return "modalwindow/user/user";
    }

    @GetMapping("/all")
    public String getAllUsers(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                              @RequestParam(defaultValue = "50", required = false) Integer countOnPage,
                                  Model model) {
        Page<User> page = userRepository.findAll(PageRequest.of(pageNum, countOnPage, new Sort("id")));
        model.addAttribute("page", page);
        return "adminpanel/userlist" ;
    }

    @GetMapping(value = "/all/json", produces = "application/json")
    @ResponseBody
    public Iterable<User> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                          @RequestParam(defaultValue = "10", required = false) Integer countOnPage){
        return userRepository.findAll(PageRequest.of(pageNum, countOnPage, new Sort("id")));
    }


    @PostMapping("/update")
    @ResponseBody
    public String update(@RequestParam Integer id,
                         @RequestParam(value = "image", required = false) MultipartFile imagePath,
                         @RequestParam String name,
                         @RequestParam Integer role,
                         @RequestParam String email,
                         @RequestParam String password){

        User user = userRepository.findById(id).get();

        if(!password.isEmpty()) user.password = password;

        if(!imagePath.isEmpty()) {
            user.image = storageService.uploadFile(imagePath, String.valueOf(user.id), storageService.USER);
        }

        user.name = name;
        user.email = email;
        user.role = roleRepository.findById(role).get();

        userRepository.save(user);

        return "ok";
    }


    @GetMapping("/update/partial")
    @ResponseBody
    public String updatePartial(@RequestParam Integer id,
                                @RequestParam(required = false) @Size(min = 3, max = 20, message = "error.size") String name,
                                @RequestParam(required = false) @Pattern(regexp = MAIL_PATTERN, message = "error.mail-incorrect") String email,
                                @RequestParam(required = false) @Size(min = 2, max = 20, message = "error.size") String password,
                                @RequestParam(required = false) String address,
                                @RequestParam(required = false) Integer index,
                                @RequestParam(required = false) String phone, Authentication authentication) {

        checkPermissions(id, authentication);
        User user = userRepository.findById(id).get();

        if (password != null) user.password = password;
        if (name != null) user.name = name;
        if (email != null) user.email = email;

        if (user.delivery == null) {
            user.delivery = new Delivery();
            user.delivery.user = user;
        }
        if (address != null) user.delivery.address = address;
        if (index != null) user.delivery.index = index;
        if (phone != null) user.delivery.phone = phone;

        userRepository.save(user);

        return "ok";
    }

    @PostMapping("/update/partial")
    @ResponseBody
    public String updatePartial(@RequestParam Integer id,
                                @RequestParam(value = "image") MultipartFile imagePath,
                                Authentication authentication) {
        checkPermissions(id, authentication);
        User user = userRepository.findById(id).get();

        if (!imagePath.isEmpty()) {
            user.image = storageService.uploadFile(imagePath, String.valueOf(user.id), storageService.USER);
            userRepository.save(user);
        }


        return user.image;
    }

    @GetMapping("/update/partial/delivery")
    @ResponseBody
    public String updatePartialDelivery(@RequestParam(value = "id") Integer userId,
                                        @RequestParam(required = false) String address,
                                        @RequestParam(required = false) Integer index,
                                        @RequestParam(required = false) String phone,
                                        Authentication authentication) {
        checkPermissions(userId, authentication);
        Delivery delivery = deliveryRepository.findByUserId(userId);
        if (delivery == null) {
            delivery = new Delivery();
            delivery.user = new User();
            delivery.user.id = userId;
        }

        if (address != null) delivery.address = address;
        if (index != null) delivery.index = index;
        if (phone != null) delivery.phone = phone;

        deliveryRepository.save(delivery);

        return "ok";
    }


    @PostMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam("image") MultipartFile imagePath,
                         @RequestParam String name,
                         @RequestParam Integer role,
                         @RequestParam String email,
                         @RequestParam String password){
        User user = new User();
        userRepository.save(user);

        user.id = userRepository.lastInsertedId();
        user.name = name;
        user.email = email;
        user.role = roleRepository.findById(role).get();
        user.password = password;
        if (imagePath.isEmpty()) user.image = "/imgstore/user/default.jpeg";
        else
            user.image = storageService.uploadFile(imagePath, String.valueOf(user.id), storageService.USER);

        userRepository.save(user);
        return "ok";
    }

    private void checkPermissions(Integer userId, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        if (user == null || !user.id.equals(userId)) throw new SecurityException();
    }

    @GetMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Integer id){
        User user = new User();
        user.id = id;
        userRepository.delete(user);
        return "ok";
    }

}