package com.repository;

import com.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    // TODO в многопоточном режиме может работать не так, как ожидалось
    @Query(value = "SELECT LAST_INSERT_ID();", nativeQuery = true)
    Integer lastInsertedId ();

    User findByEmail(String email);


}