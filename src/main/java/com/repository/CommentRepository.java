package com.repository;

import com.model.Book;
import com.model.Comment;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CommentRepository extends PagingAndSortingRepository<Comment, Integer> {
    List<Comment> findByOrderByDateDesc();

    List<Comment> findByBookOrderByDateDesc(Book book);
}