package com.repository;

import com.model.New;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NewRepository extends PagingAndSortingRepository<New, Integer> {

    @Query(value = "SELECT LAST_INSERT_ID();", nativeQuery = true)
    Integer lastInsertedId ();

    Page<New> findByOrderById(Pageable pageable);

}