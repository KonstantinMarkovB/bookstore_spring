package com.repository;

import com.model.ImageItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ImagesRepository extends PagingAndSortingRepository<ImageItem, Integer> {
    public List<ImageItem> findByOrderByIdDesc();

    @Query(value = "SELECT LAST_INSERT_ID();", nativeQuery = true)
    Integer lastInsertedId ();

    List<ImageItem> findByLabel(String label);

    Page<ImageItem> findByLabelOrderById(String label, Pageable pageable);
}