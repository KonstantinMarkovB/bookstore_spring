package com.model;


import javax.persistence.*;

@Entity
@Table(name = "delivery")
public class Delivery {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "delivery_id")
    public Integer id;

    @OneToOne
    @JoinColumn(name = "user_id")
    public User user;

    public String address;

    @Column(name = "`index`")
    public Integer index;

    public String phone;

}
