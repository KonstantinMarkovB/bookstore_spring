package com.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "new")
public class New {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "new_id")
    public Integer id;

    public String title;

    public String content;

    public Date date;

    public String image;
}
