package com.model;

import javax.persistence.*;

@Entity
@Table(name = "genre")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "genre_id")
    public Integer id;

    public String label;

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof BookGenre) {
            return id.equals(((BookGenre) o).genre.id);
        }

        return id.equals(((Genre) o).id);
    }
}
