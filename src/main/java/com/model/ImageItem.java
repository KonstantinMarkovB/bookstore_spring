package com.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jdk.nashorn.internal.ir.annotations.Ignore;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class ImageItem {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "image_id")
    public Integer id;

    @JsonIgnore
    @Column(name = "label")
    public String label;

    @Column(name = "path")
    public String image;

    @Column
    @Ignore
    public String href;

}
