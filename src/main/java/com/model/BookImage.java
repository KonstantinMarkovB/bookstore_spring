package com.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "book_images")
public class BookImage {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "image_id")
    public Integer id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "book_id")
    public Book book;

    @Column(name = "path")
    public String path;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookImage bookImage = (BookImage) o;
        return Objects.equals(id, bookImage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
