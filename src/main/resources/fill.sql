USE `book_store`;

# -- gallery role user delivery book image author order comment contact new 

INSERT INTO `role`
(`label`  ) VALUES
('ADMIN'  ),
('MANAGER'),
('USER'   );

INSERT INTO `user` 
(`name`,              `email`,             `password`,  `image`,                  `role_id`) VALUES
('admin',             'admin',             'admin',     '/imgstore/user/1.jpeg',  1        ),
('manager',           'manager',           'manager',   '/imgstore/user/2.jpeg',  2        ),
('user',              'user',              'user',      '/imgstore/user/3.jpeg',  3        ),
('Sam Winchester',    'sam.winch@mail.ru', 'samWinch',  '/imgstore/user/4.jpeg',  3        ),
('Ника Совинаш',      'nika@mail.ru',      'nika98',    '/imgstore/user/5.jpeg',  3        ),
('John Cena',         'john.cena@mail.ru', 'scalaD96',  '/imgstore/user/6.jpeg',  3        ),
('Клоков Дмитрий',    'klok.nik@mail.ru',  'klok',      '/imgstore/user/7.jpeg',  3        ),
('Аандрей Скоромный', 'scoromny@mail.ru',  'scoromny',  '/imgstore/user/8.jpeg',  3        ),
('Михаил Кокляев',    'kokliaev@mail.ru',  'kokliaev',  '/imgstore/user/9.jpeg',  3        ),
('Михаил Боратов',    'boratov.f@mail.ru', 'boratov23', '/imgstore/user/10.jpeg', 3        );

INSERT INTO `delivery` 
(`user_id`, `address`,                        `index`, `phone`             ) VALUES 
(1,         'Гомель, Ленина, 34',             1243134, '+333-(43)-23-43-345'), 
(2,         'Смоленская 23',                  1654257, '+375-(29)-45-52-356'), 
(3,         'Минск, Горького, 4',             2653556, '+333-(43)-45-27-345'), 
(4,         'Орша, Восточная, 32',            6753456, '+333-(43)-45-64-567'), 
(5,         'Орша, Ленина, 12',               6743575, '+375-(29)-67-43-453'), 
(6,         'Витебск, пр-т Фрунзе, 22',       3583567, '+333-(43)-45-96-689'), 
(7,         'Гомель, Молодежная, 23/4',       3568856, '+333-(43)-23-94-345'), 
(8,         'Брест, Смоленская, 13',          8365356, '+375-(29)-27-54-942'), 
(9,         'Витебск, пр-т Московский, 15/6', 4765534, '+375-(29)-72-34-274'), 
(10,        'Могилев, Столичная, 3',          1243145, '+375-(29)-35-12-486');

INSERT INTO `author`
(`name`             )VALUES
('М. Ю. Лермонтов'  ),
('Ф. М. Достоевский'),
('А. И. Куприн'     ),
('Н. А. Некрасов'   ),
('А. А. Блок'       ),
('С. А. Есенин'     ),
('А. А. Фет'        ),
('А. С. Пушкин'     ),
('Л. Н. Толстой'    ),
('М. Горький'       );
INSERT INTO `genre`
(`label`              ) VALUES
('Классика'           ),
('Современная проза'  ),
('Поэзия, драматургия'),
('Детские'            ),
('Бизнес'             ),
('Романы'             ),
('Детективы'          ),
('Фантастика'         ),
('Фентези'            );

INSERT INTO `book` 
(`name`,                     `author_id`, `description`, `price`, `count`, `date`     ) VALUES
('Евгений Онегин',           8,           'description', 1234,     50    , '2017/5/12'),
('Сказка о царе Салтане',    8,           'description', 3456,     40    , '2017/5/12'),
('Барышня-крестьянка',       8,           'description', 2356,     60    , '2017/5/12'),
('Герой нашего времени',     1,           'description', 4563,     75    , '2017/5/12'),
('Маскарад',                 1,           'description', 1232,     40    , '2017/5/12'),
('Демон. Сборник',           1,           'description', 1230,     65    , '2017/5/12'),
('Братья Карамазовы',        2,           'description', 4230,     23    , '2017/5/12'),
('Идиот',                    2,           'description', 3245,     60    , '2017/5/12'),
('Униженные и оскорбленные', 2,           'description', 2310,     50    , '2017/5/12'),
('Яма',                      3,           'description', 5430,     100   , '2017/5/12');

INSERT INTO `book_genre` 
(`book_id`, `genre_id` ) VALUES
(1,         1          ),
(1,         2          ),
(2,         1          ),
(2,         2          ),
(3,         1          ),
(4,         1          ),
(4,         2          ),
(5,         1          ),
(6,         1          ),
(7,         1          ),
(8,         1          ),
(9,         1          ),
(10,        1          );


INSERT INTO `book_images` 
(`book_id`, `path`                         ) VALUES
(1,         '/imgstore/book/1_1.jpeg'      ),
(1,         '/imgstore/book/1_2.jpeg'      ),
(1,         '/imgstore/book/1_3.jpeg'      ),
(2,         '/imgstore/book/2_1.jpeg'      ),
(2,         '/imgstore/book/2_2.jpeg'      ),
(2,         '/imgstore/book/2_3.jpeg'      ),
(3,         '/imgstore/book/3_1.jpeg'      ),
(3,         '/imgstore/book/3_2.jpeg'      ),
(3,         '/imgstore/book/3_3.jpeg'      ),
(4,         '/imgstore/book/4_1.jpeg'      ),
(4,         '/imgstore/book/4_2.jpeg'      ),
(4,         '/imgstore/book/4_3.jpeg'      ),
(5,         '/imgstore/book/5_1.jpeg'      ),
(5,         '/imgstore/book/5_2.jpeg'      ),
(5,         '/imgstore/book/5_3.jpeg'      ),
(6,         '/imgstore/book/6_1.jpeg'      ),
(6,         '/imgstore/book/6_2.jpeg'      ),
(6,         '/imgstore/book/6_3.jpeg'      ),
(7,         '/imgstore/book/7_1.jpeg'      ),
(7,         '/imgstore/book/7_2.jpeg'      ),
(7,         '/imgstore/book/7_3.jpeg'      ),
(8,         '/imgstore/book/8_1.jpeg'      ),
(8,         '/imgstore/book/8_2.jpeg'      ),
(8,         '/imgstore/book/8_3.jpeg'      ),
(9,         '/imgstore/book/9_1.jpeg'      ),
(9,         '/imgstore/book/9_2.jpeg'      ),
(9,         '/imgstore/book/9_3.jpeg'      ),
(10,        '/imgstore/book/10_1.jpeg'     ),
(10,        '/imgstore/book/10_2.jpeg'     ),
(10,        '/imgstore/book/10_3.jpeg'     );

INSERT INTO `comment` 
(`book_id`, `user_id`, `content`                    , `date`) VALUES
(1,         2,         'Ребята, это хит!'           , '2019/3/21 13.12'),
(1,         3,         'Огромное вам спасибо!'      , '2019/3/21 13.12'),
(1,         4,         'аж до мурашек...'           , '2019/3/21 13.12'),
(1,         5,         'Ант большой молодец.'       , '2019/3/21 13.12'),
(2,         1,         'Спасибо Вам!!!'             , '2019/3/21 13.12'),
(3,         3,         'Как долго я ждал эту книгу.', '2019/3/21 13.12'),
(4,         4,         'Приятно видеть!'            , '2019/3/21 13.12'),
(5,         1,         'Не совсем понял...'         , '2019/3/21 13.12'),
(6,         2,         'Давно ждал.'                , '2019/3/21 13.12'),
(7,         3,         'Крутая книга !!!! Спасибо!' , '2019/3/21 13.12'),
(null,      2,         'Хороший магазин. Продавец предложил несколько вариантов товара.' , '2019/3/21 13.12'),
(null,      3,         'В полном восторге от команды магазина. Идеальный сервис и максимальный выбор: товаров, способов оплаты, вариантов доставки.' , '2019/3/21 13.12'),
(null,      4,         'Буду советовать этот магазин всем. Недорого, Быстро, Качественно!', '2019/3/21 13.12');


INSERT INto `order` 
(`user_id`, `book_id`, `price`, `date`)           VALUES
(1,         1,         1235,    '2019/3/23 10.07'),
(2,         2,         5324,    '2019/4/24 14.45'),
(3,         3,         1234,    '2019/5/25 15.56'),
(4,         5,         3142,    '2019/6/26 18.11'),
(4,         2,         5320,    '2019/7/27 13.54'),
(4,         3,         1420,    '2019/8/28 11.12'),
(1,         5,         3214,    '2019/9/29 15.34'),
(1,         1,         3241,    '2019/1/20 18.23'),
(5,         1,         2013,    '2019/2/27 12.42');


INSERT INTO `contact` 
(`address`,             `schedule`,                   `phone`,             `google_lat`, `google_ing`) VALUES
('Минск, Горького, 12', 'с 10:00 до 21:00 (пн - вс)', '+7(495) 134-23-20', '23,34546',   '45,45546'  );


INSERT INTO `new`
(`title`,                                           `content`, `date`,             `image`               ) VALUES 
('В коллекцию поступили новые книги жанра комедия', 'content', '2018.11.13 13.00', '/imgstore/new/1.jpeg'),
('В коллекцию поступили новые книги жанра ужасы',   'content', '2019.01.6 13.00',  '/imgstore/new/2.jpeg'),
('Будьте в тренде',                                 "<a href='/book/card.html?id=1'>Евгений Онегин</a><br><a href='/book/card.html?id=2'>Сказка о царе Салтане</a>", '2019.02,12 13.00', '/imgstore/new/3.jpeg'),
('Привет, весна!',                                   "Скидки до -40% на: <br> <a href='/book/card.html?id=1'>Евгений Онегин</a><br>Скидки до -80% на:<br> <a href='/book/card.html?id=2'>Сказка о царе Салтане</a>", '2019.03.12 13.00', '/imgstore/new/4.jpeg'),
('ВНИМАНИЕ, ПОКУПАТЕЛЯМ!',                          'content', '2019.04.9 13.00',  '/imgstore/new/5.jpeg'),
('Прошла неделя скидок.',                           'content', '2019.04.12 13.00', '/imgstore/new/6.jpeg');

INSERT INTO `images` 
(`label`,      `path`                      , href) VALUES 
('gallery',    '/imgstore/gallery/1.jpg'   , '#'),
('gallery',    '/imgstore/gallery/2.jpg'   , '#'),
('gallery',    '/imgstore/gallery/3.jpg'   , '#'),
('gallery',    '/imgstore/gallery/4.jpg'   , '#'),
('gallery',    '/imgstore/gallery/5.jpg'   , '#'),
('gallery',    '/imgstore/gallery/6.jpg'   , '#'),
('gallery',    '/imgstore/gallery/7.jpg'   , '#'),
('gallery',    '/imgstore/gallery/8.jpg'   , '#'),
('gallery',    '/imgstore/gallery/9.jpg'   , '#'),
('gallery',    '/imgstore/gallery/10.jpg'  , '#'),
('mainSlider', '/imgstore/mainslider/1.png', '/new/get?id=3'),
('mainSlider', '/imgstore/mainslider/2.png', '#'),
('mainSlider', '/imgstore/mainslider/3.png', '/catalog.html'),
('mainSlider', '/imgstore/mainslider/4.png', '/new/get?id=4');


